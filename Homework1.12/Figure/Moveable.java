package Figure;

public interface Moveable {
    void move (double x, double y);
}
