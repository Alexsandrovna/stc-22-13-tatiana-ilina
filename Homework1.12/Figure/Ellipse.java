package Figure;

public class Ellipse extends Figure {
    private double radius1;
    private double radius2;
    public Ellipse(double x, double y) {
        super(x, y);
    }

    @Override
    public double getPerimetr() {
        double radius1Kvadrat = Math.pow(getRadius1(),2);
        double radius2Kvadrat = Math.pow(getRadius2(),2);
        double podKornem = (radius1Kvadrat+radius2Kvadrat)/2;
        double p = 2*Math.PI*Math.sqrt(podKornem);
        return p;
    }

    @Override
    public void printPerimetr() {
        System.out.println("Периметр эллипса = " + this.getPerimetr());
    }

    public double getRadius1() {
        return radius1;
    }

    public void setRadius1(double radius1) {
        this.radius1 = radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    public void setRadius2(double radius2) {
        this.radius2 = radius2;
    }
}
