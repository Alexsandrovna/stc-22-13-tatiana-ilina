package Figure;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Figure [] figures = new Figure[4];

        Rectangle rectangle = new Rectangle(4, 8);
        rectangle.setShirina(6);
        rectangle.setDlina(12);
        figures [0] = rectangle;

        Square square = new Square(6,3);
        square.setStorona(7);
        figures [1] = square;

        Ellipse ellipse = new Ellipse(3,4);
        ellipse.setRadius1(2);
        ellipse.setRadius2(4);
        figures[2]= ellipse;

        Circle circle = new Circle(3,8);
        circle.setRadius(5);
        figures[3]=circle;

        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            figures[i].printPerimetr();
            if (figures[i] instanceof Moveable) {
                double x = random.nextDouble()*20;
                figures[i].setX(x);
                double y = random.nextDouble()*20;
                figures[i].setY(y);
                System.out.println("Новые координаты " + figures[i].getX() + " "+ figures[i].getY());
            }
        }
    }
}
