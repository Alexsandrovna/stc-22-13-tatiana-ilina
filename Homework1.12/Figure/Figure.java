package Figure;

public abstract class Figure {
    private double x=0;
    private double y=0;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getPerimetr ();
    public abstract void printPerimetr ();

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
