package Figure;

public class Square extends Rectangle implements Moveable {
    private double storona;


    public Square(double x, double y) {
        super(x, y);
    }

    @Override
    public double getPerimetr() {
        double p = getStorona() * 4;
        return  p;
    }

    @Override
    public void printPerimetr() {
        System.out.println("Периметр квадрата = " + this.getPerimetr());
    }

    @Override
    public void move(double x, double y) {

    }

    public double getStorona() {
        return storona;
    }

    public void setStorona(double storona) {
        this.storona = storona;
    }
}
