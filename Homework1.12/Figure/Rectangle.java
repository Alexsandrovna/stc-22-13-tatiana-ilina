package Figure;

public class Rectangle extends Figure {
    private double dlina;
    private double shirina;

    public Rectangle(double x, double y) {
        super(x, y);
    }


    public double getPerimetr() {
        double p = (getDlina()  + getShirina()) * 2;
        return p;
    }

    @Override
    public void printPerimetr() {
        System.out.println("Периметр прямоугольника = " + this.getPerimetr());
    }

    public double getDlina() {
        return dlina;
    }

    public void setDlina(double dlina) {
        this.dlina = dlina;
    }

    public double getShirina() {
        return shirina;
    }

    public void setShirina(double shirina) {
        this.shirina = shirina;
    }
}
