package Figure;

public class Circle extends Ellipse implements Moveable {
        private double radius;

        public Circle(double x, double y) {
                super(x, y);
        }

        @Override
        public void move(double x, double y) {

        }

        @Override
        public double getPerimetr() {
                double p = 2* Math.PI*getRadius();
                return p;
        }

        @Override
        public void printPerimetr() {
                System.out.println("Периметр круга = " + this.getPerimetr());
        }

        public double getRadius() {
                return radius;
        }

        public void setRadius(double radius) {
                this.radius = radius;
        }
}
