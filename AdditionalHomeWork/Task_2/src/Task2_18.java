import java.util.Scanner;

public class Task2_18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int val = scanner.nextInt();
        int result1 = val * val;
        int result2 = val * val * val;
        System.out.println(result1 + " & " + result2);

    }
}
