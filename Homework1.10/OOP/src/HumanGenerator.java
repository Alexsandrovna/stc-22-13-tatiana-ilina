import java.util.Random;

public class HumanGenerator {
    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[random.nextInt(10)];
        //Заполняем массив
        for (int i = 0; i < humans.length; i++) {
            int age = random.nextInt(100);
            humans[i]=new Human("Tatiana", "Ilina", age);
            humans[i].printInfo();
        }
        // Сортируем массив по возрасту
        for (int i = 0; i < humans.length; i++){
            for (int j=i+1; j < humans.length; j++){
                if (humans[i].getAge() < humans[j].getAge()){
                    Human temp = humans[i];
                    humans[i] = humans [j];
                    humans[j]= temp;
                }
            }
        }

// Вывод массива на печать после сортировки
        System.out.println( "Результат" );
        for (int i = 0; i < humans.length; i++){
            humans[i].printInfo();
        }
    }

}

