import java.util.Random;

public class LinearComplexity {
    public static void main(String[] args)
    {
        int[] groupOfNumbers = new int[1000000];
        zapolnitMassiv(groupOfNumbers);
        int result= polychitSamoeRedkoeChislo(groupOfNumbers);
        System.out.println("Самое редкое число " + result);

        int[] groupOfNumbers2 = new int[100000000];
        zapolnitMassiv(groupOfNumbers2);
        result= polychitSamoeRedkoeChislo(groupOfNumbers2);
        System.out.println("Самое редкое число " + result);
    }

    //Метод, который заполняет массив случайными числами в диапазоне от -100 до 100
    //O(n) - линейная сложность
    public static void zapolnitMassiv(int[] array)
    {
        Random generatorChisel = new Random();
        for (int i = 0; i < array.length; i++)
        {
            //это случайное число от 0 до 200 например 38 ли 159
            int sluchainoeChislo;
            sluchainoeChislo = generatorChisel.nextInt(201);
            sluchainoeChislo = sluchainoeChislo - 100;
            array[i] = sluchainoeChislo;
        }
        array[array.length-1]=-101;
    }

    public static int polychitSamoeRedkoeChislo(int [] array)
    {
        //массив хранит колличество совпадений.
        // для числа -100 используется ячейка 0, для -99 - ячейка 1 и т.д.
        int []sovpadeniya=new int[201];

        int i = 0;
        while (array[i] !=-101)
        {
            int chislo=array[i];
            sovpadeniya[chislo+100]++;
            i++;
        }

        int naimencheeChisloPovtorenij=sovpadeniya[0];
        int samoeRedkoeChislo=-100;
        for (int j = 1; j<201; j++)
        {
            if (sovpadeniya[j]<naimencheeChisloPovtorenij)
            {
                naimencheeChisloPovtorenij = sovpadeniya[j];
                samoeRedkoeChislo= j-100;
            }
        }
        System.out.println("Число совпадений " + naimencheeChisloPovtorenij);
        return samoeRedkoeChislo;
    }
}
