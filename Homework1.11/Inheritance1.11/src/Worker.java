public abstract class Worker {
    protected String name; // имя
    protected String lastName; // фамилия
    protected String profession; // профессия worker

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public void goToWork() {

    }

    public void goToVacation(int days) {

    }
}
