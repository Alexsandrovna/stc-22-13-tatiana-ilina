public class Director extends Worker {
    public Director(String name, String lastName) {
        super(name, lastName, "Директор");
    }

    @Override
    public void goToWork() {
        //кто работает, какая у него профессия и как он работает
        System.out.println("Работает: " + name + " " + lastName + " " + profession + " График с 9 до 17 ");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println("Выходит в отпуск " + name + " " + lastName + " " + profession + " на " + days + " дней ");

    }
}
