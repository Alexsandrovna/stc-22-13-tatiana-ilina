public class Zavych extends Worker {
    public Zavych(String name, String lastName) {
        super(name, lastName, "Завуч");
    }

    @Override
    public void goToWork() {
        //кто работает, какая у него профессия и как он работает
        System.out.println("Работает: " + name + " " + lastName + " " + profession + " График с 9.30 до 18 ");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println("Выходит в отпуск " + name + " " + lastName + " " + profession + " на " + days + " дней ");

    }
}
