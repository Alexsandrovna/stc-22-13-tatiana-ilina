public class Secretar extends Worker {
    public Secretar(String name, String lastName) {
        super(name, lastName, "Секретарь");
    }

    @Override
    public void goToWork() {
        //кто работает, какая у него профессия и как он работает
        System.out.println("Работает: " + name + " " + lastName + " " + profession + " График с 8 до 16 ");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println("Выходит в отпуск " + name + " " + lastName + " " + profession + " на " + days + " дней ");

    }
}
