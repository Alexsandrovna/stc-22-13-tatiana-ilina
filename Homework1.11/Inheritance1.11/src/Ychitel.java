public class Ychitel extends Worker {
    public Ychitel(String name, String lastName) {
        super(name, lastName, "Учитель");
    }

    @Override
    public void goToWork() {
        //кто работает, какая у него профессия и как он работает
        System.out.println("Работает: " + name + " " + lastName + " " + profession + " График с 8.30 до 16 ");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println( "Выходит в отпуск " + name + " " + lastName + " " + profession + " на " + days + " дней ");

    }
}
