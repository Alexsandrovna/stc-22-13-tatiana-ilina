public class Inheritance {
    public static void main(String[] args) {
        Director director = new Director("Вася", "Васин");
        director.goToWork();
        director.goToVacation(31);
        Secretar secretar = new Secretar("Ирина", "Борисова");
        secretar.goToWork();
        secretar.goToVacation(28);
        Ychitel ychitel = new Ychitel("Наташа", "Королёва");
        ychitel.goToWork();
        ychitel.goToVacation(47);
        Zavych zavych = new Zavych("Ирина", "Дубцова");
        zavych.goToWork();
        zavych.goToVacation(48);

    }
}